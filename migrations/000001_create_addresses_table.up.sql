CREATE TABLE IF NOT EXISTS addresses (
    id SERIAL PRIMARY KEY,
    district TEXT NOT NULL,
    street TEXT NOT NULL,
    postcode TEXT NOT NULL
);