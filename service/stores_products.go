package service

import (
	"context"

	pb "gitlab.com/store_service/genproto/store"
	pbp "gitlab.com/store_service/genproto/product"
	"gitlab.com/store_service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *StoreService) AddProductsToStores(c context.Context, req *pb.PostStoresProducts) (*pb.GetStoreProductsInfo, error) {
	_, err := s.Client.ProductService().CreateProduct(context.Background(), &pbp.Product{
		Name: "new product",
		Model: "new model",
		TypeId: 1,
		CategoryId: 2,
	})
	if err != nil {
		s.Logger.Error("Error whlile adding products to stores", logger.Any("insert", err))
		return &pb.GetStoreProductsInfo{}, status.Error(codes.Internal, "Please recheck your datacorrectness")
	}

	return &pb.GetStoreProductsInfo{}, nil
}

func (s *StoreService) GetStoreProductById(c context.Context, req *pb.Id) (*pb.StoreProductInfo, error) {
	res, err := s.Storage.Store().GetStoreProductById(req)
	if err != nil {
		s.Logger.Error("ERrro while getting storeProduct", logger.Any("Get", err))
		return &pb.StoreProductInfo{}, status.Error(codes.Internal, "not found")
	}
	
	return res, nil
}

func (s *StoreService) GetStoreProductsByIds(c context.Context, req *pb.Ids) (*pb.GetStoreProductsInfo, error) {
	response := &pb.GetStoreProductsInfo{}
	for _, id := range req.Ids {
		res, err := s.Storage.Store().GetStoreProductById(&pb.Id{Id: id})
		if err != nil {
			s.Logger.Error("ERrro while getting storeProduct", logger.Any("Get", err))
			return &pb.GetStoreProductsInfo{}, status.Error(codes.Internal, "not found")
		}
		response.Sps = append(response.Sps, res)
	}
	return response, nil
}
