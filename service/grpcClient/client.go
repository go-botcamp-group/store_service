package grpcclient

import (
	"fmt"

	"gitlab.com/store_service/config"
	productPb "gitlab.com/store_service/genproto/product"
	"google.golang.org/grpc"
)

type ServiceManager struct {
	conf           config.Config
	productService productPb.ProductServiceClient
}

func New(cnfg config.Config) (*ServiceManager, error) {
	connProduct, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cnfg.ProductServiceHost, cnfg.ProductServicePort),
		grpc.WithInsecure(),
	)

	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d", 
		cnfg.ProductServiceHost, cnfg.ProductServicePort)
	}

	serviceManager := &ServiceManager{
		conf: cnfg,
		productService: productPb.NewProductServiceClient(connProduct),
	}

	return serviceManager, nil
}

func (s *ServiceManager) ProductService() productPb.ProductServiceClient {
	return s.productService
}