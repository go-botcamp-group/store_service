package main

import (
	"net"

	"gitlab.com/store_service/config"
	pb "gitlab.com/store_service/genproto/store"
	"gitlab.com/store_service/pkg/db"
	"gitlab.com/store_service/pkg/logger"
	"gitlab.com/store_service/service"
	grpcclient "gitlab.com/store_service/service/grpcClient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "store_service")
	defer logger.CleanUp(log)

	log.Info("main: sqlx Config",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database: %v", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("error while connect to clients", logger.Error(err))
	}

	storeService := service.NewStoreService(grpcClient, connDb, log)

	lis, err := net.Listen("tcp", cfg.GRPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterStoreServiceServer(s, storeService)
	log.Info("Server is running", logger.String("Port", cfg.GRPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
