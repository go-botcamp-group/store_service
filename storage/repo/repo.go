package repo

import pb "gitlab.com/store_service/genproto/store"

type StoreStorageI interface {
	// adddress crud
	CreateAddresses(*pb.Addresses) (*pb.Addresses, error)
	GetAddressesById(*pb.Id) (*pb.Address, error)
	UpdateAddress(*pb.Address) error
	DeleteAddressById(*pb.Id) error

	// stores crud
	CreateStore(*pb.Store) (*pb.StoreInfo, error)
	GetStoreById(*pb.Id) (*pb.StoreInfo, error)

	// stores products
	AddProductsToStores(*pb.PostStoresProducts) (*pb.GetStoreProductsInfo, error)
	GetStoreProductById(*pb.Id) (*pb.StoreProductInfo, error)
}
