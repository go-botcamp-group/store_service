package postgres

import (
	pb "gitlab.com/store_service/genproto/store"
)

func (s *StoreRepo) AddProductsToStores(req *pb.PostStoresProducts) (*pb.GetStoreProductsInfo, error) {
	response := &pb.GetStoreProductsInfo{}
	for _, sp := range req.Sps {
		tempInfo := &pb.StoreProductInfo{}
		err := s.Db.QueryRow(`insert into stores_products(store_id, product_id, price, amount) 
		values($1, $2, $3, $4)`, sp.StoreId, sp.ProductId, sp.Price, sp.Amount).Err()
		if err != nil {
			return &pb.GetStoreProductsInfo{}, err
		}
		tempInfo.Id = sp.StoreId
		var a_id int64
		err = s.Db.QueryRow(`select name, address_id from stores where id = $1`, sp.StoreId).Scan(
			&tempInfo.Name, &a_id,
		)
		if err != nil {
			return &pb.GetStoreProductsInfo{}, err
		}
		tempInfo.Address, err = s.GetAddressesById(&pb.Id{Id: a_id})
		if err != nil {
			return &pb.GetStoreProductsInfo{}, err
		}
		tempInfo.Products, err = s.GetProductsByIds(&pb.Ids{Ids: []int64{sp.ProductId}}, sp.StoreId)
		if err != nil {
			return &pb.GetStoreProductsInfo{}, err
		}
		response.Sps = append(response.Sps, tempInfo)
	}
	return response, nil
}

func (s *StoreRepo) GetStoreProductById(store_id *pb.Id) (*pb.StoreProductInfo, error) {
	response := &pb.StoreProductInfo{}
	var a_id int64
	err := s.Db.QueryRow(`SELECT id, name, address_id from stores where id = $1`, store_id.Id).Scan(
		&response.Id, &response.Name, &a_id,
	)
	if err != nil {
		return &pb.StoreProductInfo{}, err
	}
	response.Address, err = s.GetAddressesById(&pb.Id{Id: a_id})
	if err != nil {
		return &pb.StoreProductInfo{}, err
	}

	productIds := &pb.Ids{}
	rows, err := s.Db.Query(`SELECT product_id from stores_products where store_id = $1`, response.Id)
	if err != nil {
		return &pb.StoreProductInfo{}, err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&a_id)
		productIds.Ids = append(productIds.Ids, a_id)
	}
	response.Products, err = s.GetProductsByIds(productIds, response.Id)
	return response, err
}

func (p *StoreRepo) GetProductsByIds(req *pb.Ids, store_id int64) ([]*pb.ProductInStoreInfo, error) {
	response := []*pb.ProductInStoreInfo{}
	for _, id := range req.Ids {
		temp := pb.ProductInStoreInfo{}
		var c_id, t_id int64
		err := p.Db.QueryRow(`SELECT 
		p.id, 
		name, 
		model, 
		category_id, 
		type_id,
		amount,
		price 
		FROM products p
		join stores_products sp on p.id=sp.product_id 
		where p.id = $1 and sp.store_id = $2`, id, store_id).Scan(
			&temp.Id,
			&temp.Name, &temp.Model, &c_id, &t_id, &temp.Amount, &temp.Price,
		)
		if err != nil {
			return response, err
		}

		temp.Category, err = p.GetCategoryById(c_id)
		if err != nil {
			return []*pb.ProductInStoreInfo{}, err
		}
		temp.Type, err = p.GetTypeById(t_id)
		if err != nil {
			return []*pb.ProductInStoreInfo{}, err
		}
		response = append(response, &temp)
	}
	return response, nil
}

func (p *StoreRepo) GetTypeById(id int64) (*pb.Type, error) {
	res := &pb.Type{}
	err := p.Db.QueryRow(`SELECT id, name FROM types where id = $1`, id).Scan(
		&res.Id,
		&res.Name,
	)
	return res, err
}

func (p *StoreRepo) GetCategoryById(id int64) (*pb.Category, error) {
	res := &pb.Category{}
	err := p.Db.QueryRow(`SELECT id, name FROM categories WHERE id = $1`, id).Scan(
		&res.Id, &res.Name,
	)
	return res, err
}
